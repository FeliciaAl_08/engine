﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform yesLOD;
    public Transform noLOD;
    private GameObject[] yesLODs;
    private GameObject[] noLODs;
    private bool done = false;
    private bool ynLOD = false;
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ynLOD = !ynLOD;
            done = false;

            if (ynLOD)
            {
                yesLODs = GameObject.FindGameObjectsWithTag("Respawn");

                foreach (GameObject yesLOD in yesLODs)
                {
                    GameObject.Destroy(yesLOD);
                }
            }
            else if (!ynLOD)
            {
                noLODs = GameObject.FindGameObjectsWithTag("Respawn");

                foreach (GameObject noLOD in noLODs)
                {
                    GameObject.Destroy(noLOD);
                }
            }
            
        }

        if (!done)
        {
                if (ynLOD)
                {
                    for (float i = -15; i <= 15; i+=2)
                    {
                        for (float m = 15; m >= -15; m-=2)
                            Instantiate(yesLOD, new Vector3(i, 1.22f, m), Quaternion.identity);
                    }
                    done = true;
                }
                else if (!ynLOD)
                {
                    for (float i = -15; i <= 15; i+=2)
                    {
                        for (float m = 15; m >= -15; m-=2)
                            Instantiate(noLOD, new Vector3(i, 1.22f, m), Quaternion.identity);
                    }
                    done = true;
                }
        }
    }
}
