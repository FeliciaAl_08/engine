﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Listener : MonoBehaviour
{
    public GameObject m_player;
    Rigidbody m_RB;

    [DllImport("Fmod_Plugin")]
    public static extern void updateListener(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateListenerVel(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateListenerUp(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateListenerFor(float x, float y, float z);
    // Use this for initialization
    void Start ()
    {
        m_RB = m_player.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        updateListener(transform.position.x, transform.position.y, transform.position.z);
        updateListenerVel(m_RB.velocity.x, m_RB.velocity.y, m_RB.velocity.z);
        updateListenerUp(transform.up.x, transform.up.y, transform.up.z);
        updateListenerFor(transform.forward.x, transform.forward.y, transform.forward.z);
    }
}
