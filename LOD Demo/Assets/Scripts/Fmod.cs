﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Fmod : MonoBehaviour
{
    bool Play = false;
    [DllImport("Fmod_Plugin")]
    public static extern void initialize();

    [DllImport("Fmod_Plugin")]
    public static extern void update();

    [DllImport("Fmod_Plugin")]
    public static extern void createSounds(string s);

    [DllImport("Fmod_Plugin")]
    public static extern void playSound();
    // Use this for initialization
    void Start ()
    {
        initialize();
        createSounds("C:/Users/100527945/Documents/Engines Tutorials/Game Engines Lab 1-2/Assets/Sounds/Mr Meeseeks Sounds.wav");
	}

    void OnCollisionEnter()
    {
        Debug.Log("hi");
        playSound();
        Play = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (Play)
        {
            update();
        }
	}
}
