﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MyEditorWindow : EditorWindow
{
    string m_String = "Type Stuff Here";
    float m_SlideControl = 0;
    bool m_Checked = true;
    bool m_Group;
    [MenuItem("Component/MyEditor")]
    public static void ShowWindow()
    {
        //If not created create one on menu item click
        EditorWindow.GetWindow(typeof(MyEditorWindow));
    }

	void OnGUI()
    {
        //Creating label named settings
        GUILayout.Label("Settings", EditorStyles.miniBoldLabel);

        m_String = EditorGUILayout.TextField("Text Field", m_String);
        m_Group = EditorGUILayout.BeginToggleGroup("Optional", m_Group);
        m_SlideControl = EditorGUILayout.Slider("Slider", m_SlideControl, -10, 10);
        m_Checked = EditorGUILayout.Toggle("CheckBox", m_Checked);
        EditorGUILayout.EndToggleGroup();

    }
}
