﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Fmod : MonoBehaviour
{
    bool Play;

    [DllImport("Fmod_Plugin")]
    public static extern void initialize();

    [DllImport("Fmod_Plugin")]
    public static extern void update();

    [DllImport("Fmod_Plugin")]
    public static extern void createSounds(string s);

    [DllImport("Fmod_Plugin")]
    public static extern void playSound();

    // Use this for initialization
    void Start()
    {
        initialize();
        createSounds("C:/Users/100568964/Documents/Fmod/Assets/Sounds/baap.wav");

    }

    void OnCollisionEnter()
    {
        playSound();
        Play = true;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
