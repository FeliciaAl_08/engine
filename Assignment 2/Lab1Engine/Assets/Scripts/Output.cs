﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;
using UnityEngine;

public class Output : MonoBehaviour
{
    [DllImport("HelloWorld")]
    public static extern IntPtr HelloWorld();

    [DllImport("HelloWorld")]
    public static extern void Log(string CharName, string ItemName, string Value);
    // Use this for initialization
    void Start()
    {
        Debug.Log(Marshal.PtrToStringAnsi(HelloWorld()));
        //Log(gameObject.transform.name,"Transform",transform.ToString());
    }
    void Update()
    {
        Log(gameObject.transform.name, "Transform", transform.position.ToString() + " Time: " + Time.time);
    }
}
